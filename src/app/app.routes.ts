import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register.component';
import { TemplateComponent } from './templates/generi-template/template.component';

// Se ha implementado Lazy loading 

const appRoutes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    {
        path: 'PM',
        component: TemplateComponent,
        children: [
            {
                path: 'common',
                loadChildren: './pages/common/common.module#CommonModule'             
            },
            {
                path: 'product',    
                loadChildren: './pages/products/products.module#ProductModule'
            }
        ]
    },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },

];

export const APP_ROUTES = RouterModule.forRoot(appRoutes, { useHash: true });
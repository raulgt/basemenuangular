import{Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'filtercurrency'
})
export class FilterCurrencyPipe implements PipeTransform{
    transform(value: any, args: any[]): string{       
        
        if(typeof(value) == 'number'){
            return '$ '+ value;
        }else{
            return value;
        }
        
    }
}
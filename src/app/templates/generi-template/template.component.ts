import { Component, OnInit } from '@angular/core';

declare function init_plugins();

@Component({
  selector: 'admin-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    init_plugins();
  }

}

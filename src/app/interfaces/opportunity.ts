
export interface IJobOpportunity {
   open : boolean;    
   lastModification : String;
   jobLink : String;
   user : String;
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.service';
import { IUserInfo } from '../interfaces/userInfo';
import { ToasterService } from '../services/toaster.service';

//declare function init_plugins();


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formGroup: FormGroup;

  
  constructor(private fb: FormBuilder,
    private accountService: AccountService,
    private router: Router,
    private toast: ToasterService) {

  }

  ngOnInit() {
   // init_plugins();
    
    this.formGroup = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]     
    });
  }


  loguearse() {
    let userInfo: IUserInfo = Object.assign({}, this.formGroup.value);
    this.accountService.login(userInfo).subscribe(token => {
      if(token.result == null || !token.isSuccess){
        this.toast.Error('Error', 'Password o usuario inválido.!');
        return;
      }
      console.log(token.result);
      this.recibirToken(token.result);
    },
      error => this.manejarError(error));
  }

  recibirToken(token) {
    localStorage.setItem('token', token.token);
    localStorage.setItem('tokenExpiration', token.expiration);
    this.router.navigate(["/PM/common/home/"]);
  }

  manejarError(error) {
    if (error && error.error) {
      this.toast.Error('Error', error.toString());
    }
  }
}

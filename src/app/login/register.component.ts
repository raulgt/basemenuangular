import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.service';
import { IUserInfo } from '../interfaces/userInfo';
import { ToasterService } from '../services/toaster.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./login.component.css']
})
export class RegisterComponent implements OnInit {

  formGroup: FormGroup;

  constructor(private fb: FormBuilder,
    private accountService: AccountService,
    private router: Router,
    private toast: ToasterService) { }

  ngOnInit() {

    this.formGroup = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      passwordConfirm: ['', Validators.required]
    });

  }

  registrarse() {

    if (this.formGroup.get('password').value !== this.formGroup.get('passwordConfirm').value) {
      this.toast.Error('Error', 'La confirmación del password no coincide');
      return;
    }

    let userInfo: IUserInfo = Object.assign({}, this.formGroup.value);
    this.accountService.create(userInfo).subscribe(token => {
      if(token.result == null || !token.isSuccess){
        this.toast.Error('Error', 'Password muy débil..!');
        return;
      }
      this.recibirToken(token.result)
    },
      error => this.manejarError(error));
  }

  recibirToken(token) {
    localStorage.setItem('token', token.token);
    localStorage.setItem('tokenExpiration', token.expiration);
    this.router.navigate(["/PM/common/home/"]);
  }

  manejarError(error) {
    if (error && error.error) {
      this.toast.Error('Error', error.toString());
    }
  }

}

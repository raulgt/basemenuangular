import {Routes, RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthGuardService } from 'src/app/services/auth-guard.service';



const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',       
    },
    {
        path: 'home',
        component: HomeComponent, 
        canActivate: [AuthGuardService]       
    } 

];

export const CommonRouting = RouterModule.forChild(routes);
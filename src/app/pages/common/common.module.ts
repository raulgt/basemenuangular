import {NgModule} from '@angular/core';
import { HomeComponent } from './home/home.component';
import { CommonRouting } from './common.route';

@NgModule({
    declarations: [ 
        HomeComponent             
    ],  
    imports: 
    [         
        CommonRouting        
    ],
    exports: [],
    providers: [],
})

export class CommonModule {}
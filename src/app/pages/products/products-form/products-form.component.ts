import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { SelectItem } from 'primeng/api';
import { ProductService } from 'src/app/services/product.service';
import { IProducto } from '../product';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DatePipe, CurrencyPipe } from '@angular/common';




@Component({
  selector: 'app-products-form',
  templateUrl: './products-form.component.html',
  styleUrls: ['./products-form.component.css']
})
export class ProductsFormComponent implements OnInit {

  @ViewChild('spinner')
  private spinner: ElementRef;

  producto: IProducto;
  fd: FormData = new FormData();

  formTitle: string = "Nuevo Producto";

  userform: FormGroup;

  countries: SelectItem[] = [];
  modoDetail: boolean = false;
  productId: number;
  initialDate: string = '';
  initialCountry: string = '';
  precioDetail: string;

  //imagen
  selectedFile: File;
  imageUrl: string = 'assets/images/gallery/deafulItem.png';
  tooBigImage: boolean = false;

  constructor(private fb: FormBuilder,
    private messageService: MessageService,
    private productService: ProductService,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private renderer: Renderer2) { }

  ngOnInit() {


    this.userform = this.fb.group({
      'nombre': new FormControl('', Validators.required),
      'caracteristicas': new FormControl('', Validators.required),
      'precio': new FormControl('', Validators.required),
      // 'fechaLanzamiento': new FormControl('', Validators.compose([Validators.required, Validators.minLength(6)])),
      'fechaLanzamiento': new FormControl('', Validators.required),
      'correoFabricante': new FormControl('', Validators.required),
      'pais': new FormControl('', Validators.required),
      'unidadesDisponibles': new FormControl('', Validators.required),
      'unidadesVendidas': new FormControl('', Validators.required),
      'fechaUltimaRevision': new FormControl(''),
      'revision': new FormControl(''),
      'imagen': new FormControl('', Validators.required),
    });


    // Se prepara el dropdown de paises
    this.countries.push({ label: 'Seleccione País', value: '' });

    this.productService.getCountries()
      .subscribe((country: any[]) => {       
        country.forEach(p => this.countries.push({ label: p.name, value: p.name }));
      }, error => console.error(error));

   
    // Se evalua si se ha navegado al modo de detall
    this.activatedRouter.params.subscribe(params => {

      if (params["id"] == undefined) {
        return;
      }

      this.modoDetail = true;
      this.productId = params["id"];

      //Cargamos el detalle de un producto
      this.productService.getProduct(this.productId.toString())
        .subscribe(product => this.chargeProduct(product.result),
          error => console.error(error));

    });
  }


 

  ////////////////////////////////////
  ////// Capturamos la imagen /////////
  ////////////////////////////////////

  onFileSelected(event) {
    this.selectedFile = <File>event.target.files[0];

    if (this.selectedFile === undefined || this.selectedFile == null) {
      this.imageUrl = 'assets/images/gallery/deafulItem.png';
      return;
    }

    this.tooBigImage = (this.selectedFile.size > 6144) ? true : false;

    if (this.tooBigImage) {
      this.imageUrl = 'assets/images/gallery/deafulItem.png';
      return;
    }

    // Mostrar la imagen del producto a crear
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.selectedFile);
  }


  // Aqui se invoca el servicio para guardar el nuevo producto
  onSubmit(value: any) {   

    this.renderer.setAttribute(this.spinner.nativeElement, 'class', 'fa fa-spinner fa-spin');

    var dp = new DatePipe('en-US');

    var format = "yyyy/MM/dd hh:mm:ss";  

    // Si existen datos previos en el FormData se crea una nueva instancia
    this.fd = new FormData();

    let producto: IProducto = Object.assign({}, this.userform.value);

    this.fd.append('imagen', this.selectedFile, this.selectedFile.name);
    this.fd.append('nombre', producto.nombre);
    this.fd.append('caracteristicas', producto.caracteristicas);
    this.fd.append('precio', producto.precio.toString());
    this.fd.append('fechaLanzamiento', dp.transform(producto.fechaLanzamiento, format));
    this.fd.append('correoFabricante', producto.correoFabricante);
    this.fd.append('pais', producto.pais);
    this.fd.append('unidadesDisponibles', producto.unidadesDisponibles.toString());
    this.fd.append('unidadesVendidas', producto.unidadesVendidas.toString());
    this.fd.append('revision', producto.revision.toString());

    this.productService.createProduct(this.fd)
      .subscribe(producto => {

        if (producto.result) {
          this.messageService.add({ severity: 'info', summary: 'Success', detail: 'Form Submitted' });
          this.onSaveSuccess();
        } else {
          this.onSaveFailed();
        }
      }, error => {
        console.error(error);
      });
  }

  // Acciones al creacr un producto
  onSaveSuccess() {
    setTimeout(() => {
      this.router.navigate(["/PM/product/products/"]);
    }, 800);
  }

  onSaveFailed() {
    alert('Ha ocurrido una falla no controlada, comuniquese con el administrador del sistema');
  }

  //////////////////////////////////////////////////////////////////////
  //////////////              MODO DETALLLE                  //////////   
  //////////////////////////////////////////////////////////////////////

  chargeProduct(product: IProducto) {

    this.formTitle = "Detalle de Producto";

    if ((!product.imagenFile) && (!product.imagenFileTable)) {
      this.imageUrl = 'assets/images/gallery/deafulItem.png';
    } else if (product.imagenFile) {
      this.imageUrl = 'data:image/jpeg;base64,' + product.imagenFile;
    } else {
      this.imageUrl = 'data:image/jpeg;base64,' + product.imagenFileTable;
    }

    var dp = new DatePipe('en-US');
    var format = "yyyy-MM-dd   h:mm:ss";

    this.userform.patchValue({
      nombre: product.nombre,
      caracteristicas: product.caracteristicas,
      precio: product.precio,
      correoFabricante: product.correoFabricante,
      unidadesDisponibles: product.unidadesDisponibles,
      unidadesVendidas: product.unidadesVendidas,
      fechaUltimaRevision: product.fechaUltimaRevision,
      revision: product.revision
    });

    this.initialCountry = product.pais;
    this.initialDate = dp.transform(product.fechaLanzamiento, format);
    this.precioDetail = new CurrencyPipe('en-US').transform(product.precio, 'USD', true, '1.4-4');
    this.userform.disable();
  }




  //  get diagnostic() {   
  //    console.log(this.userform);
  //    return JSON.stringify(this.userform.value);
  //    }

}

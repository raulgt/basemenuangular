import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { IProducto } from './product';
import { operationResult } from 'src/app/share/interfaces/operationResult';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/api';



@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {


  displayDialog: boolean = false;
  currentProductId: string;
  currentProductName: string;

  // Table data
  products: IProducto[] = [];
  datasource: IProducto[] = [];

  countries: SelectItem[] = [];
  cols: any[];
  loading: boolean;
  emptyList: boolean;

  // table paginator
  totalRecords: number = 0;
  rowNumber: number = 10;
  pagesVisited: number[] = [];
  pagescount: number = 0;



  constructor(private productsService: ProductService, private route: Router) { }

  ngOnInit() {

    this.cols = [
      { field: 'nombre', header: 'Nombre' },
      { field: 'pais', header: 'Pais' },
      { field: 'correoFabricante', header: 'Correo Fabricante' },
      { field: 'caracteristicas', header: 'Características' },
      { field: 'precio', header: 'Precio' }
    ];

    this.countries.push({ label: 'Todos', value: null });
    this.InitchargeProducts();

  }


  ///////////////////////////////////////////////////////
  /// Se cargan los primeros productos por primera vez///
  ///////////////////////////////////////////////////////

  InitchargeProducts(nextRowKey: string = 'empty', nextPartitionKey = 'empty', bunch: number = 10) {    

    this.products = [];
    this.datasource = [];
    this.pagesVisited = [];
    this.pagescount = 0;
    this.totalRecords = 0;

    this.loading = true;
    this.productsService.getProducts(nextRowKey, nextPartitionKey, bunch)
      .subscribe((products: operationResult) => {
        this.products = products.result;
        this.datasource = products.result;
        this.pagesVisited.push(this.pagescount++);

        if (this.datasource.length > 0) {
          this.totalRecords = (this.datasource[this.datasource.length - 1].nextRowKey != null) ? this.datasource.length + 1 : this.datasource.length;
           // Cargamos paises para el filtro
           this.chargeCountries();
        } else {
          // Informacion sobre lista vacia
          this.emptyList = true;
        }
        this.loading = false;
      }, error => console.error(error));
  }

   chargeCountries() {
    // Cargamos los paises para la busqueda en la tabla cuidando que no se repitan
    var exist;
    this.datasource.forEach((value) => {
        exist = this.countries.some(c => c.label == value.pais);
      if (!exist) {
        this.countries.push({ label: value.pais, value: value.pais });
      }
    });
  }

    ///////////////////////////////////////////////////////
   //////// Detalle y elimancion de un producot  /////////
  ///////////////////////////////////////////////////////

  selectProductWithButton(rowData, flat = 0) {  
    if (flat > 0) {     
      this.showDialog((rowData.id != null && rowData.id != undefined) ? rowData.id : rowData.rowKey, rowData.nombre);
    } else {

      if (rowData.id != null && rowData.id != undefined) {
        // SQL SERVER
        this.route.navigate(['/PM/product/products-detail', rowData.id.toString()]);
      } else {
        // TABLE STORAGE
        this.route.navigate(['/PM/product/products-detail', rowData.rowKey]);
      }
    }
  }

  // Eliminar un producto
  showDialog(id: string, name: string) {
    this.displayDialog = true;
    this.currentProductId = id;
    this.currentProductName = name;
  }

   // Confirmar eliminacion de un producto
  confirmDelete() {
    this.productsService.deleteProduct(this.currentProductId)
      .subscribe(resp => {
        this.displayDialog = false;
        this.currentProductId = '';
        this.currentProductName = '';
        this.InitchargeProducts();
      }, error => console.error(error));

  }

  // Abortar eliminacion de un producto
  abortDelete() {
    this.displayDialog = false;
    return;
  }

  ///////////////////////////////////////////////////////
  ///////////  Paginacion Personalizada ////////////////
  ///////////////////////////////////////////////////////

  paginate(event) {  
    if (this.pagesVisited.find(value => value == event.page) === undefined) {
      this.goOnTable(event.first, event.rows);
    } else {
      this.goPreviousTable(event.first, event.rows);
    }   
  }

  goOnTable(first: number, rows: number) {  

    let nextRowKey = this.datasource[this.datasource.length - 1].nextRowKey;
    let nextPartitionKey = this.datasource[this.datasource.length - 1].nextPartitionKey;
     // Activamos el loader
    this.loading = true;

    this.productsService.getProducts(nextRowKey, nextPartitionKey, this.rowNumber)
      .subscribe((products: operationResult) => {
        this.datasource = this.datasource.concat(products.result);  
        this.totalRecords = (this.datasource[this.datasource.length - 1].nextRowKey != null) ? this.datasource.length + 1 : this.datasource.length;
        
        // Cargamos paises nuevos en el filtro
        this.chargeCountries();
        this.pagesVisited.push(this.pagescount++);

        // Ordenar
        this.products = this.datasource.slice(first, (first + rows));
        // Quitamos el loader
        this.loading = false;
      }, error => console.error(error));
  }

  goPreviousTable(first: number, rows: number) { 
    // Ordenar
    this.products = this.datasource.slice(first, (first + rows));
  }


}

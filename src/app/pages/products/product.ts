


export interface IProducto {
    id : number;
    rowKey : string;
    nombre: string;
    caracteristicas: string;
    fechaLanzamiento: string;
    correoFabricante: string;
    pais: string;
    precio: number;
    unidadesDisponibles: string;
    unidadesVendidas: string;
    fechaUltimaRevision : string;
    revision: boolean;    
    imagen: FormData;
    imagenFile: string;    
    imagenFileTable : string;
    nextRowKey: string;
    nextPartitionKey: string;
    }
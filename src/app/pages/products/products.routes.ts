import {Routes, RouterModule} from '@angular/router';
import { ProductsComponent } from './products.component';
import { ProductsFormComponent } from './products-form/products-form.component';
import { AuthGuardService } from 'src/app/services/auth-guard.service';



const routes: Routes = [
    {
        path: '',
        redirectTo: 'products',
        pathMatch: 'full',       
    },
    {
        path: 'products',
        component: ProductsComponent,
        canActivate: [AuthGuardService]        
    },
    {
        path:'products-form', 
        component: ProductsFormComponent,
        canActivate: [AuthGuardService]
    },
    {
        path:'products-detail/:id', 
        component: ProductsFormComponent,
        canActivate: [AuthGuardService]
    },

];

export const ProductRouting = RouterModule.forChild(routes);
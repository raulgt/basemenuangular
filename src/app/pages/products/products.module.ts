import {NgModule} from '@angular/core';
import { ProductRouting } from './products.routes';
import { ProductsComponent } from './products.component';
import { ProductsFormComponent } from './products-form/products-form.component';
import {CommonModule} from '@angular/common';
import { PrimeNgModule } from 'src/app/primeng-components/primeng.module';
import { FilterCurrencyPipe } from 'src/app/pipes/customcurrency.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [ 
        ProductsComponent,
        ProductsFormComponent,
        FilterCurrencyPipe,
    ],  
    imports: 
    [ 
        ProductRouting,
        CommonModule,     
        PrimeNgModule,
        FormsModule,
        ReactiveFormsModule   
    ],
    
    exports: [],
    providers: [],
})

export class ProductModule {}
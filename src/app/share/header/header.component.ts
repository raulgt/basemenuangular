import { Component, OnInit } from '@angular/core';
import * as signalR from "@aspnet/signalr";
import { OpportunityService } from 'src/app/services/opportunity.service';
import { ToasterService } from 'src/app/services/toaster.service';
import Swal from 'sweetalert2'
import { AccountService } from 'src/app/services/account.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})


export class HeaderComponent implements OnInit {

  opportunityFlatText: string;
  currentStatus: boolean;
  private hubConnection: signalR.HubConnection;

  constructor(private opportunityService: OpportunityService,
    private toast: ToasterService,
    private accountService: AccountService,
    private router: Router) { }

  ngOnInit() {

    // Requerimos el estado inicial
    this.opportunityService.getOpportunityStatus()
      .subscribe(status => {
        this.currentStatus = status.result;
        this.opportunityFlatText = (this.currentStatus) ? 'Abierto' : 'Cerrado';
      });

    // Creamos la conexion al Hub
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl('https://localhost:44391/opportunityHub')
      .build();

    // Controlamos de forma sencilla un error de conexion
    this.hubConnection.start().catch(err => console.error(err.toString()));

    // Escuchamos los mensajes del tipo "ReceiveStatus" en donde me informan de un cambio de status
    this.hubConnection.on("ReceiveStatus", (status) => {
      this.currentStatus = status;
      this.opportunityFlatText = (this.currentStatus == true) ? 'Abierto' : 'Cerrado';
      this.toast.Info("Información", 'Cambió el estatus de ofertas de trabajo..!!');
    });

  }

  // Funcion que cambia el status de la Oprotunidad e invoca el Hub para informar 
  //  del  cambio si esta es realizado con exito.
  ChangeStatusOpportunity() {
    Swal.fire({
      title: 'Confirmar Acción?',
      text: "Desea cambiar el estatus de las oportunidades, esto afectará a todos los usuarios.!!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmado!'
    }).then((result) => {
      if (result.value) {
        this.changuingOpportunityStatus();
      }
    });

  }

  changuingOpportunityStatus() {
    this.opportunityService.changueOpportunityStatus('Carlos Abreu')
      .subscribe(status => {
        this.currentStatus = status.result;
        this.opportunityFlatText = (this.currentStatus) ? 'Abierto' : 'Cerrado';
        this.hubConnection.invoke("NotifyOpportunity", this.currentStatus).catch(err => console.error(err.toString()));
      }, error => console.error(error));
  }

  logout(){
    this.accountService.logout();
    this.router.navigate(["/login"]); 
  }

}




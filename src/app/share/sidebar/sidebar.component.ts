import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(  private accountService: AccountService,
                private router: Router) { }

  ngOnInit() {
  }

  logout(){
    this.accountService.logout();
    this.router.navigate(["/login"]); 
  }

}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IProducto } from '../pages/products/product';
import { Observable, throwError } from 'rxjs';
import { operationResult } from '../share/interfaces/operationResult';

import {delay, retryWhen, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  // SqlServer CRUD
  apiUrl: string = "https://localhost:44391/api/Products/";
  //apiUrl: string = "https://localhost:44391/api/Products/azure";

  constructor(private http: HttpClient) { }


  getProducts(nextRowKey: string = 'empty', nextPartitionKey = 'empty', bunch: number = 0): Observable<operationResult> {

    if (this.apiUrl == 'https://localhost:44391/api/Products/') {
      return this.http.get<operationResult>(this.apiUrl);
    } else {
      const headers = new HttpHeaders()
        .set('nextRowKey', nextRowKey)
        .set('nextPartitionKey', nextPartitionKey)
        .set('bunch', bunch.toString());
      return this.http.get<operationResult>(this.apiUrl, { headers: headers });
    }
  }

  getProduct(id: string): Observable<operationResult> {

    return this.http.get<operationResult>(this.apiUrl + '/' + id);
  }

  createProduct(producto: any): Observable<operationResult> {
    const header = new HttpHeaders();
    header.append('Accept', 'application/json');
    header.append('Content-Type', 'multipart/form-data');

    return this.http.post<operationResult>(this.apiUrl, producto, { headers: header });
  }

  deleteProduct(id: string): Observable<operationResult> {
    return this.http.delete<operationResult>(this.apiUrl + '/' + id);
  }

  getCountries(): Observable<any> {
    return this.http.get<any>('https://restcountries.eu/rest/v2/all').pipe(
      retryWhen((err) => {
        return err.pipe(delay(1000), take(3));
      }
      ));
  }




  //  UploadImage(img: any): Observable<operationResult> {
  //   const header = new HttpHeaders();
  //   header.append('Content-Type', 'multipart/form-data');
  //   header.append('Accept', 'application/json');
  //    return this.http.post<operationResult>(this.apiUrl+'/pruebaupload',img, {headers: header});
  //  }
}

import { Injectable } from '@angular/core';

import { AccountService } from './account.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private acountService: AccountService, private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {

    if (this.acountService.estaLogueado()) {     
      return true;
    } else {     
      this.router.navigate(['/login']);
      return false;
    }
  }
}

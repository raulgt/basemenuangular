import { Injectable } from '@angular/core';
import { IUserInfo } from '../interfaces/userInfo';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { operationResult } from '../share/interfaces/operationResult';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

   // pass: Aa123456! 
   private apiURL: string = "https://localhost:44391/api/account";

  constructor(private http: HttpClient) { }

  
  create(userInfo: IUserInfo): Observable<operationResult> {
    return this.http.post<any>(this.apiURL + "/Create", userInfo);
  }

  login(userInfo: IUserInfo): Observable<operationResult> {
    return this.http.post<any>(this.apiURL + "/Login", userInfo);
  }

  obtenerToken(): string {
    return localStorage.getItem("token");
  }

  obtenerExpiracionToken(): string {
    return localStorage.getItem("tokenExpiration");
  }

  logout() {
    localStorage.removeItem("token");
    localStorage.removeItem("tokenExpiration");
  }

  estaLogueado(): boolean {

    var exp = this.obtenerExpiracionToken();

    if (!exp) {
      // el token no existe
      return false;
    }

    var now = new Date().getTime();
    var dateExp = new Date(exp);

    if (now >= dateExp.getTime()) {
      // ya expiró el token
      localStorage.removeItem('token');
      localStorage.removeItem('tokenExpiration');
      return false;
    } else {
      return true;
    }

  }
}

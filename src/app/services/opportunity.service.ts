import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { operationResult, httpStatusCode } from '../share/interfaces/operationResult';

@Injectable({
  providedIn: 'root'
})
export class OpportunityService {

  apiUrl: string = "https://localhost:44391/api/Opportunity/";

  constructor(private http: HttpClient) { }

  getOpportunityStatus():Observable<operationResult>  {
         return this.http.get<operationResult>(this.apiUrl);
  }

   changueOpportunityStatus(user: string): Observable<operationResult> {
     return this.http.put<operationResult>(this.apiUrl + '/' + user,{});
   }

}

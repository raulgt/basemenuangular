import { Injectable } from '@angular/core';

declare var toastr: any;

@Injectable({
  providedIn: 'root'
})
export class ToasterService {

  constructor() {
    this.ToastrSetting();
   }


  Success(title: string, meassage?: string) {
    toastr.success(meassage, title);
  }

  Warning(title: string, meassage?: string) {
    toastr.warning(meassage, title);
  }

  Error(title: string, meassage?: string) {
    toastr.error(meassage, title);
  }

  Info(title: string, meassage?: string) {
    toastr.info(meassage, title);
  }

  private ToastrSetting() {

    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-bottom-right",
      "preventDuplicates": true,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "500",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
}
}

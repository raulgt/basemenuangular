import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//NGPrime
import { TableModule } from 'primeng/table';
import { SharedModule } from 'primeng/components/common/shared';
import { ToastModule } from 'primeng/toast';
import { MessageModule, MessageService } from 'primeng/primeng';
import { MessagesModule } from 'primeng/messages';
import { DropdownModule } from 'primeng/dropdown';
import { PanelModule } from 'primeng/panel';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { DialogModule } from 'primeng/dialog';
import {PaginatorModule} from 'primeng/paginator';
import {CheckboxModule} from 'primeng/checkbox';


@NgModule({
  imports: [
            CommonModule,
            TableModule,
            SharedModule,
            ToastModule,
            MessageModule,
            MessagesModule,
            DropdownModule,
            PanelModule,
            InputTextModule,
            ButtonModule,
            CalendarModule,
            DialogModule,
            PaginatorModule,
            CheckboxModule
           ],
           exports:[
            TableModule,
            SharedModule,
            ToastModule,
            MessageModule,
            MessagesModule,
            DropdownModule,
            PanelModule,
            InputTextModule,
            ButtonModule,
            CalendarModule,
            DialogModule,
            PaginatorModule,
            CheckboxModule
           ],
           providers:[MessageService]
})
export class PrimeNgModule { }
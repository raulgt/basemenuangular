import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { APP_ROUTES } from './app.routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';

//Componentes compartidos
import { HeaderComponent } from './share/header/header.component';
import { SidebarComponent } from './share/sidebar/sidebar.component';

// Login y registro
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register.component';


//Servicios
import { ProductService } from './services/product.service';
import { AccountService } from './services/account.service';
import { OpportunityService } from './services/opportunity.service';
import { ToasterService } from './services/toaster.service';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthInterceptorService } from './services/auth-interceptor.service';

//Plantilla principal
import { TemplateComponent } from './templates/generi-template/template.component';





@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent, 
    LoginComponent,
    RegisterComponent,   
    TemplateComponent,   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    APP_ROUTES,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
 
  ],
  exports:[    
    
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [
    ProductService,
     OpportunityService,
      AccountService, 
      AuthGuardService,
      ToasterService,
      {
        provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptorService,
        multi: true
      }],
  bootstrap: [AppComponent]
})
export class AppModule { }
